<?php

    /* Only run this is a CLI arguement for the data file */
    if ($argv.length > 0){
        print "\n\nERROR: Missing arguement. \nRun this command as: \n    php app.php datafilename.csv \n\n\n\n";
        stop;
    }
    else{

        require_once('data/config.inc');
        require_once('inc/helpers.inc');
        require_once('inc/cartesian.inc');

        print "\n";
        print '############################################################';
        print "\n".'    Building all combination from $dataSourceCSV'."\n";
        print '############################################################';
        print "\n\n\n";        

    
        /* Make a nested array of Platform-Candidate csv for this city */
        $dataSourceCSV              = 'data/'.$argv[1];           
        $candidateStatement         = csv_to_array($dataSourceCSV, ',');        
        $candidateStatementKeys     = array_keys($candidateStatement);
        $candidateStatementKeyCount = count($candidateStatementKeys);


        $thisCity                   = str_replace('.csv', '',$argv[1]);

        /* Source is a CSV. Get the header keys to use with each loop below */


        /* Loop through cartesian collection and grab the next row's combination */
        $cartesianOptionsMax    = count($cartesianArray);        

        for ($i = 0; $i < $cartesianOptionsMax; $i++){

            print "\n\n\n${i}: ";

            $thisCartesianCombo       = $cartesianArray[$i];              
            $thisCartesianComboLength = count($thisCartesianCombo);
            
            $thisHTMLFile             = '';    
            $thisImageFile            = '';
            
            $thisCombinationDesc      = '<dl>'; 
            $thisCombinationDesc_og   = '';
            /* Loop through thisCartesianCombo to get the selection */
            for($j = 0; $j < $thisCartesianComboLength; $j++){

                $thisCartesianComboValue = $thisCartesianCombo[$j];                

                print $thisCartesianComboValue . '-';
                
                $thisPlatformCategory    = $candidateStatement[$j]['Platform'];
                
                $thisPlatformArray       = $candidateStatement[$j];


                $thisPlatformArrayKeys   = array_keys($thisPlatformArray);                
                
                // if($thisCartesianComboValue >0 ){
                // }
                // else{
                //     $thisCandidate           = $thisPlatformArrayKeys[$thisCartesianComboValue];                    
                // }

                if($thisCartesianComboValue == 4){
                    $ThisCandidatePlatformKey =  $thisCartesianComboValue -1 ;
                }
                else{
                    $ThisCandidatePlatformKey =  $thisCartesianComboValue + 1;
                }


                $thisCandidate           = $thisPlatformArrayKeys[$ThisCandidatePlatformKey];                                        
                $thisCandidatePlatform   = $candidateStatement[$j][$thisPlatformArrayKeys[$ThisCandidatePlatformKey]];


                /* Assemble the HTML and image filename */

                $thisHTMLFile            = $thisHTMLFile . ${thisCartesianComboValue};
                $thisImageFile           = $thisImageFile . ${thisCartesianComboValue};

              //  print "\n(${j}) ";
              //  print "${thisPlatformCategory} ";
              //  print " [${thisCartesianComboValue}] ";
              //  print " ${thisCandidate}: $thisCandidatePlatform ";


                
                $thisCombinationDesc        = $thisCombinationDesc . '<dt>' . ${thisPlatformCategory} . '</dt><dd><b>' . ${thisCartesianComboValue} . ' ' .${thisCandidate} . '</b>: ' . ${thisCandidatePlatform} . '</dd>';
                $thisCombinationDesc_og     = $thisCombinationDesc_og . ${thisPlatformCategory} . ' : ' . ${thisCandidate} . ' - ' . ${thisCandidatePlatform} ;

                // print "$thisCombinationDesc";

                /* Assemble the response for this particular combination */


            }

            /* Open the HTMl file for writing and populate partials with content for this platform/candidate combination */

            $thisCombinationDesc = $thisCombinationDesc . '</dl>';

            $thisHTMLFile       = $thisHTMLFile  . '.html';
            $thisImageFile      = $thisImageFile . '.png';

            $thisOGShareImage   = $COMPOSITEIMAGEBASEDIR . $thisImageFile;

            // print "$thisCombinationDesc \n\n";

/* NO PRIDE HERE */
$thisHTMLMarkup = <<<EOT
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    
    <title>$NEWSAPPTITLE</title>

    <!-- SEO CONTENT -->
    <meta name="description" content="${NEWSAPPDESC}" />
    <meta name="keywords" content="${NEWSAPPKEYWORDS}" />
    <!-- END SEO CONTENT -->

    <!-- FACEBOOK OPENGRAPH SHARING -->
    <meta property="og:title"           content="${NEWSAPPTITLE}" />
    <meta property="og:description"     content="${thisCombinationDesc_og}" />
    <meta property="og:url"             content="${SHAREURL}" />
    <meta property="og:image"           content="${thisOGShareImage} " />
    <meta property="og:type"            content="article" />
    <!-- END FACEBOOK OPENGRAPH SHARING -->

    <!-- TWITTER CARD -->
    <meta name="twitter:site"           content="@cbcnews">
    <meta name="twitter:card"           content="summary_large_image">
    <meta name="twitter:image:src"      content="${thisOGShareImage} ">
    <meta name="twitter:description"    content="${thisCombinationDesc_og}">
    <meta name="twitter:title"          content="${NEWSAPPTITLE}">
    <meta name="twitter:url"            content="${SHAREURL}">
    <!-- END TWITTER CARD -->

    
    

</head>
<body>

    <div class="rendered-results">
        
        <h1>[CATCH: SHARE BUTTON BAR]</h1>
        <figure class="rendered-composite">
            <img src="$thisOGShareImage " width="100" alt="Make my mayor - Platform combination, details follow." />
        </figure>
        <div>            
            $thisCombinationDesc            
        </div>    

    </div>

</body>
</html>
EOT;




            $bakedHTML = fopen($OUTPUTBASEDIR.$thisCity.'/'.$thisHTMLFile, "w") or die("Unable to open file! $thisHTMLFile");






            fwrite($bakedHTML, $thisHTMLMarkup);
            fclose($bakedHTML);

            // print "\n";

        }
        /* Loop through cartesian collection and grab the next row's combination */

    }
    
    
    // print_r($candidateStatement[3]);
    // print_r($candidateStatementKeys);
    // print_r($thisPlatformArrayKeys);

?>