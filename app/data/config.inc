<?php

    $NEWSAPPTITLE          = 'Make my Mayor - CBC.ca';
    $NEWSAPPDESC           = 'Pick your political positions, see what kind of leader you can make.';
    $NEWSAPPKEYWORDS       = 'Mayor, council, election';


    $OUTPUTBASEDIR         = 'output/';
    $COMPOSITEIMAGEBASEDIR = 'http:/make-my-mayor/composite/';
    $SHAREURL              = 'http://cbc.ca/make-my-mayor/';

?>