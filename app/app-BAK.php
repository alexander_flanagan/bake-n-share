<?php

    /* Only run this is a CLI arguement for the data file */
    if ($argv.length > 0){
        print "\n\nERROR: Missing arguement. \nRun this command as: \n    php app.php datafilename.csv \n\n\n\n";
        stop;
    }
    else{

        /* Open Cartesian nested array  */
        require_once('data/cartesian.php');
        require_once('inc/helpers.php');

        print "\n";
        print '############################################################';
        print "\n".'    Building all combination from $dataSourceCSV'."\n";
        print '############################################################';
        print "\n\n\n";        

        $dataSourceCSV          = 'data/'.$argv[1];
        $cartesianOptionsMax    = count($cartesianArray);
        
        /* Make a nested array of Platform-Candidate csv for this city */
        $candidatePositions     = csv_to_array($dataSourceCSV, ',');
        $candidatePositionsKeys = array_keys($candidatePositions);
        
        /* Source is a CSV. Get the header keys to use with each loop below */



        /* Loop through each possible combination */
        for ($i = 0; $i < $cartesianOptionsMax; $i++){
            
            $cartesianOptionsCount  = count($cartesianArray[$i]);

            $thisCombination = '';


// WHAT'S THE PLATFORM?



            for ($j = 0; $j < $cartesianOptionsCount; $j++){

                /* $j is the platform and $cartesianItem is the cartesian grid item (pairing of platform:candidate) */
                $cartesianItem        = $cartesianArray[$i][$j];

                print "\n";
                // print "${j} -- ";            
                print "${cartesianItem} - ";


                // print "${j}<${cartesianOptionsCount} ";
                $thisPlatform     = $candidatePositions[$cartesianItem];

                print_r($thisPlatform);
                
                $thisPlatformKeys = array_keys($thisPlatform);
                $thisCandidate    = $thisPlatformKeys[$cartesianItem];

                print "${thisCandidate}";
                // print_r($thisPlatformKeys);
                print " --- ";
                // print_r($thisPlatform['Platform']);

                // $thisPlatformKeys = array_keys($candidatePositions[$candidatePositionsKeys[3]] );
                


                // print_r($thisPlatformKeys);
                // print($thisPlatformKeys[$j].' ');
                // print_r($thisPlatform[$thisPlatformKeys[3]);

                /* This PlatformKey */
                // print_r($thisPlatform['Platform']);


                // print_r($thisPlatform['Platform'][$thisPlatformKeys[$j]]);


                /* Separator for image index -- results in 2-3-1-2-0 format */
                if($j >= 1){            
                    $thisCombination        = $thisCombination.'-';
                }

                $thisCombination            = $thisCombination.$cartesianItem;
                


                // print_r($candidatePositionsKeys[$thisPairing]);
                // print_r($candidatePositions [$candidatePositionsKeys[$j]]);
                
                                            
                print ' < ';

                // print $cartesianArray[$i][$j];

           }



            /* Open an HTML file for writing */
            $imageFile   = $thisCombination . '.png';
            $htmlFile    = $thisCombination . '.html';
            
            print "\n${i}: ${thisCombination}";
            // print "\n${i}: Opening " . $htmlFile . " for writing .......... ";

            /* Close the file and do the next combo */

            print "\n......... Done.\n\n";
            


            // print_r($thisPlatformKeys);

        }
        /* Loop through each possible combination */





    }
    
    



?>