<?php

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    
    <title><?php print $NEWSAPPTITLE; ?></title>

    <!-- SEO CONTENT -->
    <meta name="description" content="<?php print $NEWSAPPDESC; ?>" />
    <meta name="keywords" content="<?php print $NEWSAPPKEYWORDS; ?>" />
    <!-- END SEO CONTENT -->

    <!-- FACEBOOK OPENGRAPH SHARING -->
    <meta property="og:title"           content="<?php print $NEWSAPPTITLE; ?>" />
    <meta property="og:description"     content="<?php print $thisCombinationDesc; ?>" />
    <meta property="og:url"             content="<?php print $SHAREURL; ?>" />
    <meta property="og:image"           content="<?php print $thisOGShareImage ; ?>" />
    <meta property="og:type"            content="article" />
    <!-- END FACEBOOK OPENGRAPH SHARING -->

    <!-- TWITTER CARD -->
    <meta name="twitter:site"           content="@cbcnews">
    <meta name="twitter:card"           content="summary_large_image">
    <meta name="twitter:image:src"      content="<?php print $thisOGShareImage ; ?>">
    <meta name="twitter:description"    content="<?php print $thisCombinationDesc; ?>">
    <meta name="twitter:title"          content="<?php print $NEWSAPPTITLE; ?>">
    <meta name="twitter:url"            content="<?php print $SHAREURL; ?>">
    <!-- END TWITTER CARD -->

</head>
<body>

    <div class="rendered-results">
        <figure>
            <img src="<?php print $thisOGShareImage ; ?>" />
        </figure>
        <div>            
            <?php print $thisCombinationDesc; ?>
        </div>    

    </div>

</body>
</html>

<?php

?>