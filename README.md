Bake-n-Share
==========


Bake out static pages for sharing all possible iterations of user selection. 


First build for Make My Mayor -- candidates and platform positions.


Data source files are CSV and kept in data/*


    cd src
    php app.php datafilename.csv



e.g., toronto.csv 


The data csv has the following convention
"Platform","Candidate 1","Candidate 2","Candidate 3","Candidate 4"
"Taxes", "Candidate 1 position", "Candidate 2 position", "Candidate 3 position", "Candidate 4 position"


data/config.inc has some strings and paths used throughout. 


HTML files are backed to output/CITYNAME/00000.html. CITYNAME directory needs to exist first.







